package bean

type User struct {
	Id                int64  `json:"id" form:"id" gorm:"column:id"`
	UserID            string `json:"user_id" gorm:"column:user_id"`
	Account           string `json:"test" gorm:"column:account"` // 用于保存BD,实际上这俩是一样的.
	UserName          string `json:"username" form:"username" gorm:"column:username"`
	Password          string `json:"password" form:"password" gorm:"column:password"`
	ImgUrl            string `json:"img_url" form:"img_url" gorm:"column:img_url"`
	Seq               int    `json:"seq" form:"seq" gorm:"column:seq"`
	Email             string `json:"email" form:"email" gorm:"column:email"` // email.
	City              string `json:"city" form:"city" gorm:"column:city"`
	HaveNoReadMessage int    `json:"haveNoReadMessage" gorm:"-"`
	CreateTime        int64  `json:"create_time" gorm:"autoCreateTime;column:create_time"`
	LastUpdateTime    int64  `json:"last_update_time" form:"last_update_time" gorm:"column:last_update_time;autoUpdateTime"`
}
