package xcommon

import (
	"strings"

	"github.com/google/uuid"
)

// GetUUID return uuid.
func GetUUID() string {
	return strings.ReplaceAll(uuid.New().String(), "-", "")
}
