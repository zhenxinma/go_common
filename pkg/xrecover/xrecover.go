package xrecover

import (
	"runtime/debug"

	"gitee.com/zhenxinma/go_common/pkg/xstring"

	"gitee.com/zhenxinma/go_common/pkg/xlogging"
)

const deBugStackLogLength = 4096

// GoroutineRecover goroutine panic recover.
func GoroutineRecover(xlog *xlogging.Entry) {
	defer func() {
		if err := recover(); err != nil {
			debugInfo := debug.Stack()
			if len(debugInfo) > deBugStackLogLength {
				// 左闭右开.
				debugInfo = debugInfo[:deBugStackLogLength]
			}
			xlog.Errorf("panic: %v", xstring.SliceByteToString(debugInfo))
		}
	}()
}
