// Package authserver
// Create  2023-04-05 21:24:54
package authserver

import (
	"context"
	"gitee.com/zhenxinma/gocommon/pkg/bean"
)

// AuthArgs 鉴权参数.
type AuthArgs struct {
	UserID string `json:"user_id"`
}

// AuthResponse 鉴权响应.
type AuthResponse struct {
	UserInfo *bean.User `json:"user_info"`
}

// AuthServer 健全服务.
type AuthServer interface {
	// GetAuthInfo 获取鉴权信息.
	GetAuthInfo(c context.Context, arg *AuthArgs, rsp *AuthResponse) error
}
