package xstring

import (
	"fmt"
	"unsafe"
)

// string([]byte("str")) 先将str拷贝一份出来,然后在转换成字节数组.

// go string 底层结构.
//type stringStruct struct {
//	array unsafe.Pointer  // 指向一个 [len]byte 的数组
//	length int             // 长度
//}

//type SliceHeader struct {
//	Data uintptr
//	Len int
//	Cap int
//}

// StringToSliceByte 更加高效的字符串转字节切片.
func StringToSliceByte(str string) []byte {
	p := unsafe.Pointer(&str)
	x := (*[2]uintptr)(p)
	h := [3]uintptr{x[0], x[1], x[1]}
	return *(*[]byte)(unsafe.Pointer(&h))
}

// SliceByteToString 更加高效的字节切片转字符串.
func SliceByteToString(data []byte) string {
	ptr := (*string)(unsafe.Pointer(&data))
	fmt.Println(ptr)
	// *pointer读取这个指针的内容.
	return *ptr
}
