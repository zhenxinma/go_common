// Package common 不便于分类的通用方法.
// @author: zhenxinma.
// @create: 2021-10-08 23:52:33
package common

import (
	"runtime"
	"strings"

	"github.com/google/uuid"
)

// GetOsSeparator return cur os sep.
func GetOsSeparator() string {

	s := runtime.GOOS
	if strings.Contains(s, "win") {
		return "\\"
	} else {
		return "/"
	}
}

// GetUUID return req_id
func GetUUID() string {
	return strings.ReplaceAll(uuid.New().String(), "-", "")
}
