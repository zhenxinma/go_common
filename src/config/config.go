// Package config: 对外提供解析配置文件的能力.
// @author: mazhenxin.
// @create: 2021-09-14 16:35:00
package config

import (
	"log"
	"sync"

	"gitee.com/zhenxinma/gocommon/src/common_static"

	"github.com/spf13/viper"
)

var _config *viper.Viper

var once = &sync.Once{}

// Init 初始配置,采用默认的配置.
// 如果别的模块进行引入的话,那么其加载的配置文件应该在同一个地方.
func Init() {
	once.Do(initConfig)
}

// initConfig 确保只会执行一次,也就是_config确保只会赋值一次.
func initConfig() {

	// 设置配置文件所在的目录.
	viper.SetConfigName(common_static.DefaultConfigFileName)
	// 设置配置文件的类型.
	viper.SetConfigType(common_static.DefaultConfigFileType)
	// 设置配置文件的目录.
	viper.AddConfigPath(common_static.DefaultConfigPath)
	// 动态监听配置文件.
	go func() {
		viper.WatchConfig()
	}()
	err := viper.ReadInConfig()
	if nil != err {
		log.Fatalln("init config error: ", err.Error())
	}
	// 赋值.
	_config = viper.GetViper()
}

// GetConfig 对外提供Config配置能力.
func GetConfig() *viper.Viper {
	if _config == nil {
		// 如果为空,则进行初始化.
		Init()
		return _config
	}
	return _config
}

// TODO: 如果还需要其他解析配置文件的模式,可以在通过配置进行添加
