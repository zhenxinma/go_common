package main

import "fmt"

// @author: mazhenxin.
// @create: 2021-09-13 15:19:37
// https://gitee.com/zhenxinma/go_base.git

func main() {
	fmt.Println("Hello Word")
}

//func main() {
//
//	// 原始字符串.
//	// 加密的key?
//	key := "123456781234567812345678"
//	//fmt.Println("原文：", orig)
//
//	// 加密.
//	encryptCode := common_encode.AesEncrypt(orig, key)
//	fmt.Println("密文：", encryptCode)
//
//	// 解密结果.
//	decryptCode := common_encode.AesDecrypt(encryptCode, key)
//	fmt.Println("解密结果：", decryptCode)
//}
